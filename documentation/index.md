# sobytes.aws Module

## Description

TODO: Enter your module description here

## Accessing the sobytes.aws Module

To access this module from JavaScript, you would do the following:

    var sobytes_aws = require("com.sobytes.sobytesaws");

The sobytes_aws variable is a reference to the Module object.

## Reference

TODO: If your module has an API, you should document
the reference here.

### sobytes_aws.function

TODO: This is an example of a module function.

### sobytes_aws.property

TODO: This is an example of a module property.

## Usage

TODO: Enter your usage example here

## Author

TODO: Enter your author name, email and other contact
details you want to share here.

## License

TODO: Enter your license/legal information here.
