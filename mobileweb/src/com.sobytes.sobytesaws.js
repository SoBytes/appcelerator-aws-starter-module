define(["Ti/_/lang"], function(lang) {

	return lang.setObject("sobytes.aws", {
		example: function() {
			return "hello world";
		},

		properties: {
			exampleProp: "hello world"
		}
	});

});